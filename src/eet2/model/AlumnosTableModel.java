/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.model;

import eet2.io.Alumno;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author root
 */
public class AlumnosTableModel extends AbstractTableModel {

    private static final String[] COLUMNAS = {"DNI", "Apellido", "Nombre"};
    private List<Alumno> alumnos;

    //Creacion de una nueva instancia de Tabla de Actores
    public AlumnosTableModel(List<Alumno> alumnos) {
        super();
        this.alumnos = alumnos;
    }

    @Override
    public int getRowCount() {
        return alumnos == null ? 0 : alumnos.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    //El metodo siguiente es el encargado de completar los datos de cada celda
    @Override
    public Object getValueAt(int fila, int columna) {
        Object retorno = null;
        Alumno alumno = alumnos.get(fila);
        // según la columna deseada obtenemos el valor a mostrar
        switch (columna) {
            case 0:
                retorno = alumno.getDni();
                break;
            case 1:
                retorno = alumno.getApellido();
                break;
            case 2:
                retorno = alumno.getNombre();
                break;
        }
        return retorno;
    }

    /**
     * Devuelve el nombre de la columna en columnIndex . Esto se usa para
     * inicializar el nombre del encabezado de columna de la tabla. Nota: este
     * nombre no necesita ser único; dos columnas en una tabla pueden tener el
     * mismo nombre.
     *
     * @param index el índice de la columna
     * @return el nombre de la columna
     */
    @Override
    public String getColumnName(int index) {
        return COLUMNAS[index];
    }

    public Alumno obtenerAlumnoSeleccionado(int fila) {
        return alumnos.get(fila);
    }

    public void actualizarListadoAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

}
