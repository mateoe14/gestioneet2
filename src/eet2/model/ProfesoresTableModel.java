/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.model;

import eet2.io.Profesor;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author root
 */
public class ProfesoresTableModel extends AbstractTableModel {
    
    private static final String[] COLUMNAS = {"DNI", "Apellido", "Nombre"};
    private List<Profesor> profesores;

    //Creacion de una nueva instancia de Tabla de Profesores
    public ProfesoresTableModel(List<Profesor> profesores) {
        super();
        this.profesores = profesores;
    }

    @Override
    public int getRowCount() {
        return profesores == null ? 0 : profesores.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    //El metodo siguiente es el encargado de completar los datos de cada celda
    @Override
    public Object getValueAt(int fila, int columna) {
        Object retorno = null;
        Profesor profesor = profesores.get(fila);
        // según la columna deseada obtenemos el valor a mostrar
        switch (columna) {
            case 0:
                retorno = profesor.getDni();
                break;
            case 1:
                retorno = profesor.getApellido();
                break;
            case 2:
                retorno = profesor.getNombre();
                break;
        }
        return retorno;
    }

    /**
     * Devuelve el nombre de la columna en columnIndex . Esto se usa para
     * inicializar el nombre del encabezado de columna de la tabla. Nota: este
     * nombre no necesita ser único; dos columnas en una tabla pueden tener el
     * mismo nombre.
     *
     * @param index el índice de la columna
     * @return el nombre de la columna
     */
    @Override
    public String getColumnName(int index) {
        return COLUMNAS[index];
    }

    public Profesor obtenerProfesorSeleccionado(int fila) {
        return profesores.get(fila);
    }

    public void actualizarListadoProfesores(List<Profesor> profesores) {
        this.profesores = profesores;
    }    
    
}
