/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.model;

import eet2.io.Articulo;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author root
 */
public class PedidosTableModel1 extends AbstractTableModel {

    private static final String[] COLUMNAS = {"Nombre", "Stock"};
    private List<Articulo> articulos;

    //Creacion de una nueva instancia de Tabla de Actores
    public PedidosTableModel1(List<Articulo> articulos) {
        super();
        this.articulos = articulos;
    }

    @Override
    public int getRowCount() {
        return articulos == null ? 0 : articulos.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    //El metodo siguiente es el encargado de completar los datos de cada celda
    @Override
    public Object getValueAt(int fila, int columna) {
        Object retorno = null;
        Articulo articulo = articulos.get(fila);
        // según la columna deseada obtenemos el valor a mostrar
        switch (columna) {
            case 0:
                retorno = articulo.getNombre();
                break;
            case 1:
                retorno = articulo.getStock();
                break;
        }
        return retorno;
    }

    /**
     * Devuelve el nombre de la columna en columnIndex . Esto se usa para
     * inicializar el nombre del encabezado de columna de la tabla. Nota: este
     * nombre no necesita ser único; dos columnas en una tabla pueden tener el
     * mismo nombre.
     *
     * @param index el índice de la columna
     * @return el nombre de la columna
     */
    @Override
    public String getColumnName(int index) {
        return COLUMNAS[index];
    }

    public Articulo obtenerArticuloSeleccionado(int fila) {
        return articulos.get(fila);
    }

    public void actualizarListadoArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

}
