/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.model;

import eet2.io.Taller;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author root
 */
public class TalleresTableModel extends AbstractTableModel {

    private static final String[] COLUMNAS = {"Año", "Especialidad", "Profesor"};
    private List<Taller> talleres;

    //Creacion de una nueva instancia de Tabla de Actores
    public TalleresTableModel(List<Taller> talleres) {
        super();
        this.talleres = talleres;
    }

    @Override
    public int getRowCount() {
        return talleres == null ? 0 : talleres.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    //El metodo siguiente es el encargado de completar los datos de cada celda
    @Override
    public Object getValueAt(int fila, int columna) {
        Object retorno = null;
        Taller taller = talleres.get(fila);
        // según la columna deseada obtenemos el valor a mostrar
        switch (columna) {
            case 0:
                retorno = taller.getAñoCurso()+"° Año";
                break;
            case 1:
                retorno = taller.getNombre();
                break;
            case 2:
                try {
                retorno = taller.getProfesor().toString();
                } catch (Exception e) {}
                break;
        }
        
        return retorno;
    }

    /**
     * Devuelve el nombre de la columna en columnIndex . Esto se usa para
     * inicializar el nombre del encabezado de columna de la tabla. Nota: este
     * nombre no necesita ser único; dos columnas en una tabla pueden tener el
     * mismo nombre.
     *
     * @param index el índice de la columna
     * @return el nombre de la columna
     */
    @Override
    public String getColumnName(int index) {
        return COLUMNAS[index];
    }

    public Taller obtenerTallerSeleccionado(int fila) {
        return talleres.get(fila);
    }

    public void actualizarListadoTalleres(List<Taller> talleres) {
        this.talleres = talleres;
    }

}
