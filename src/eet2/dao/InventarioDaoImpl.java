/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Articulo;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class InventarioDaoImpl implements InventarioDao {

    private final SessionFactory sessionFactory;
    
    public InventarioDaoImpl (SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void agregar(Articulo unArticulo) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(unArticulo);
        session.getTransaction().commit();
        session.close();
    }
    
    @Override
    public void modificar(Articulo unArticulo) {
        
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(unArticulo);
        session.getTransaction().commit();
        session.close();
    }
    
    @Override
    public void eliminar(Articulo unArticulo) {
        
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(unArticulo);
        session.getTransaction().commit();
        session.close();        
    }    

    @Override
    public Articulo buscarPorId(int id) {
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       Articulo articulo = (Articulo) session.get(Articulo.class, id);
       session.getTransaction().commit();
       session.close();
       
       return articulo;
    }
    
    @Override
    public List<Articulo> buscarPorNombre(String nombre) {
       
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Articulo> articulos = session.createSQLQuery("SELECT * FROM inventario WHERE nombre LIKE '%"+nombre+"%'").addEntity(Articulo.class).list();
       session.getTransaction().commit();
       session.close();
       return articulos;
    }   
    
    @Override
    public List<Articulo> listar() {
        
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Articulo> articulos = session.createSQLQuery("SELECT * FROM inventario ORDER BY nombre ASC").addEntity(Articulo.class).list();
       session.getTransaction().commit();
       session.close();
        
       return articulos;
    }    
        
}
