/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Profesor;
import java.util.List;

/**
 *
 * @author root
 */
public interface ProfesoresDao {
    
    public void agregar(Profesor unProfesor);
    public void modificar(Profesor unProfesor);
    public void eliminar(Profesor unProfesor);
    public List<Profesor> buscarPorNombre(String nombre);
    public List<Profesor> buscarPorApellido(String apellido);
    public Profesor buscarPorId(int dni);
    public List<Profesor> listar();
    
}
