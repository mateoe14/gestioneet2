/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Alumno;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class AlumnosDaoImpl implements AlumnosDao {
    
    private final SessionFactory sessionFactory;
    
    public AlumnosDaoImpl (SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;        
    }

    @Override
    public void agregar(Alumno unAlumno) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(unAlumno);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void modificar(Alumno unAlumno) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(unAlumno);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void eliminar(Alumno unAlumno) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(unAlumno);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Alumno buscarPorId(int dni) {
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       Alumno alumno = (Alumno) session.get(Alumno.class, dni);
       session.getTransaction().commit();
       session.close();
       
       return alumno;
    }

    @Override
    public List<Alumno> buscarPorNombre(String nombre) {
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Alumno> alumnos = session.createSQLQuery("SELECT * FROM alumnos WHERE nombre LIKE '%"+nombre+"%'").addEntity(Alumno.class).list();
       session.getTransaction().commit();
       session.close();
       
       return alumnos;
    }

    @Override
    public List<Alumno> buscarPorApellido(String apellido) {
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Alumno> alumnos = session.createSQLQuery("SELECT * FROM alumnos WHERE apellido LIKE '%"+apellido+"%'").addEntity(Alumno.class).list();
       session.getTransaction().commit();
       session.close();
       
       return alumnos;
    }

    @Override
    public List<Alumno> listar() {
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Alumno> alumnos = session.createSQLQuery("SELECT * FROM alumnos ORDER BY apellido ASC").addEntity(Alumno.class).list();
       session.getTransaction().commit();
       session.close();
       
       return alumnos;
    }
    
}
