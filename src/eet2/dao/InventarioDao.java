/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Articulo;
import java.util.List;

/**
 *
 * @author root
 */
public interface InventarioDao {
    
    public void agregar(Articulo unArticulo);
    public void modificar(Articulo unArticulo);
    public Articulo buscarPorId (int id);
    public List<Articulo> buscarPorNombre (String nombre);
    public List<Articulo> listar();
    public void eliminar(Articulo unArticulo);
}
