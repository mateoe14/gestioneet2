/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Profesor;
import eet2.io.Taller;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class ProfesoresDaoImpl implements ProfesoresDao{
    
    private final SessionFactory sessionFactory;
    
    public ProfesoresDaoImpl (SessionFactory sessionFactory){
        
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void agregar(Profesor unProfesor) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(unProfesor);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void modificar(Profesor unProfesor) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(unProfesor);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void eliminar(Profesor unProfesor) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(unProfesor);
        session.getTransaction().commit();
        session.close();
    }
    
    @Override
    public List<Profesor> buscarPorNombre(String nombre) {
       
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Profesor> profesores = session.createSQLQuery("SELECT * FROM profesores WHERE nombre LIKE '%"+nombre+"%'").addEntity(Profesor.class).list();
       session.getTransaction().commit();
       session.close();
       
       return profesores;
    }
    
    @Override
    public List<Profesor> buscarPorApellido(String apellido) {
       
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Profesor> profesores = session.createSQLQuery("SELECT * FROM profesores WHERE apellido LIKE '%"+apellido+"%'").addEntity(Profesor.class).list();
       session.getTransaction().commit();
       session.close();
       return profesores;
    }
    
    @Override
    public Profesor buscarPorId(int dni) {
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       Profesor profesor = (Profesor) session.get(Profesor.class, dni);
       session.getTransaction().commit();
       session.close();
       
       return profesor;
    }

    @Override
    public List<Profesor> listar() {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        List<Profesor> profesores = session.createSQLQuery("SELECT * FROM profesores ORDER BY apellido ASC").addEntity(Profesor.class).list();
        session.getTransaction().commit();
        session.close();
        
        return profesores;
    }
          
}
