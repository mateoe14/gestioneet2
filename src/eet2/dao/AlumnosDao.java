/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Alumno;
import java.util.List;

/**
 *
 * @author root
 */
public interface AlumnosDao {
    
    public void agregar(Alumno unAlumno);
    public void modificar(Alumno unAlumno);
    public void eliminar (Alumno unAlumno);
    public Alumno buscarPorId(int dni);
    public List<Alumno> buscarPorNombre(String nombre);
    public List<Alumno> buscarPorApellido(String apellido);
    public List<Alumno> listar();
    
}
