/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Taller;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class TalleresDaoImpl implements TalleresDao {

    private final SessionFactory sessionFactory;
    
    public TalleresDaoImpl (SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void agregar(Taller unTaller) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(unTaller);
        session.getTransaction().commit();
        session.close();
    }
    
    @Override
    public void modificar(Taller unTaller) {
        
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(unTaller);
        session.getTransaction().commit();
        session.close();
    }
    
    @Override
    public void eliminar(Taller unTaller) {
        
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(unTaller);
        session.getTransaction().commit();
        session.close();
        
    }
    
    @Override
    public Taller buscarPorId(int id) {
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       Taller taller = (Taller) session.get(Taller.class, id);
       session.getTransaction().commit();
       session.close();
       
       return taller;
    }    
    
    @Override
    public List<Taller> buscarPorNombre(String nombre) {
       
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Taller> talleres = session.createSQLQuery("SELECT * FROM talleres WHERE nombre LIKE '%"+nombre+"%'").addEntity(Taller.class).list();
       session.getTransaction().commit();
       session.close();
       return talleres;
    }
    
    @Override
    public List<Taller> buscarPorAño(int año) {
       
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Taller> talleres = session.createSQLQuery("SELECT * FROM talleres WHERE año_curso = "+año).addEntity(Taller.class).list();
       session.getTransaction().commit();
       session.close();
       
       return talleres;
    } 
    
    @Override
    public List<Taller> listar() {
        
       Session session = sessionFactory.openSession();
       session.beginTransaction();
       List<Taller> talleres = session.createSQLQuery("SELECT * FROM talleres ORDER BY año_curso ASC").addEntity(Taller.class).list();
       session.getTransaction().commit();
       session.close();
        
       return talleres;
    }   
            
}
