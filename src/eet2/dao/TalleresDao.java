/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.dao;

import eet2.io.Taller;
import java.util.List;

/**
 *
 * @author root
 */
public interface TalleresDao {
    
    public void agregar(Taller unTaller);
    public void modificar(Taller unTaller);
    public void eliminar(Taller unTaller);
    public Taller buscarPorId(int id);
    public List<Taller> buscarPorNombre (String nombre);
    public List<Taller> buscarPorAño (int año);
    public List<Taller> listar ();
}
