/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.main;

import eet2.controller.GestorPrincipal;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.metamodel.MetadataSources;

/**
 *
 * @author root
 */
public class EET2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        SessionFactory sessionFactory = null;
        	
    try {
        sessionFactory = new Configuration().configure().buildSessionFactory();
        }
    catch (Exception e) {
            System.out.println("Error al crear la instancia de SessionFactory: " + e.getMessage());
	
        }
            new GestorPrincipal().run(sessionFactory);
    }
    
    
}
