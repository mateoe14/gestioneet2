/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.io;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author root
 */

@Entity
@Table (name = "profesores")
public class Profesor implements Serializable {
    @Basic(optional = false)
    
    @Id
    private int dni;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "apellido")
    private String apellido;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy="profesor", orphanRemoval = false)
    private List<Taller> talleres;
    
    public Profesor() {
        this.talleres = new ArrayList<>();
    }
        
    public Profesor (int dni, String nombre, String apellido){
        
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.talleres = new ArrayList<>();
        
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public List<Taller> getTalleres() {
        
        try{
        this.talleres.size();} catch (Exception e) {}
        return talleres;
    }

    public void addTaller(Taller taller) {   
        talleres.add(taller);  
    }    
  
    public void setTalleres(List<Taller> talleres) {
            this.talleres = talleres;
    }
        
    @Override
    public String toString() {
        return this.nombre+" "+this.apellido;
    }    
    
}
