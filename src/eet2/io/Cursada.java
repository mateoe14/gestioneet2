/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.io;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author root
 */
@Entity
@Table(name="cursadas")
public class Cursada implements Serializable {
    @Id
    private int id;
    
    @ManyToOne//(mappedBy = "cursadas")
    private Alumno alumno;
    
    @ManyToOne
    private Taller taller;
    
    @Column(name="cicloLectivo")
    private int cicloLectivo;
    
    public Cursada(){}
    
    public Cursada(Alumno alumno, Taller taller, int cicloLectivo){
        
        this.alumno = alumno;
        this.taller = taller;
        this.cicloLectivo = cicloLectivo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Taller getTaller() {
        return taller;
    }

    public void setTaller(Taller taller) {
        this.taller = taller;
    }

    public int getCicloLectivo() {
        return cicloLectivo;
    }

    public void setCicloLectivo(int cicloLectivo) {
        this.cicloLectivo = cicloLectivo;
    }    
    
}
