/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.io;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author root
 */

@Entity
@Table (name = "alumnos")
public class Alumno implements Serializable {
    @Basic(optional = false)
    
    @Id
    private int dni;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "apellido")
    private String apellido;
    
    @OneToMany(mappedBy = "alumno")
    @JoinColumn (name="dniAlumno")
    private List<Cursada> cursadas;
    
    @OneToMany(mappedBy = "alumno")
    @JoinColumn (name="dniAlumno")
    private List<Pedido> pedidos;
    
    public Alumno (){}
    
    public Alumno (int dni, String nombre, String apellido){
    
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cursadas = new ArrayList<>();
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public List<Cursada> getCursadas() {
        return cursadas;
    }

    public void setCursadas(List<Cursada> cursadas) {
        this.cursadas = cursadas;
    }    

    public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }
    
}
