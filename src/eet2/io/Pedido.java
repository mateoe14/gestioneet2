/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.io;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author root
 */
@Entity
@Table(name="pedidos")
public class Pedido implements Serializable{    
    @Basic(optional = false)
    
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private int id;
    
    @Column(name = "fechaPedido")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaPedido;
    
    @Column(name = "fechaDevol")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaDevol;
    
    @Column(name = "cantidad")
    @Temporal(javax.persistence.TemporalType.DATE)
    private int cantidad;
    
    @ManyToOne
    private Articulo articulo;
    
    @ManyToOne
    private Alumno alumno;
    
    @ManyToOne
    private Taller taller;
    
    public Pedido (){}
    
    public Pedido (Date fechaPedido, Articulo articulo, Alumno alumno, Taller taller){
        
        this.fechaPedido = fechaPedido;
        this.fechaDevol = null;
        this.articulo = articulo;
        this.alumno = alumno;
        this.taller = taller;
        
    }

    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public Date getFechaDevol() {
        return fechaDevol;
    }

    public void setFechaDevol(Date fechaDevol) {
        this.fechaDevol = fechaDevol;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Taller getTaller() {
        return taller;
    }

    public void setTaller(Taller taller) {
        this.taller = taller;
    }
    
    
}
