/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.io;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;

/**
 *
 * @author root
 */
@Entity
@Table(name="Inventario")
public class Articulo implements Serializable {
    @Basic(optional = false)
    
    //ATRIBUTOS
    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private int id;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "stock")
    private int stock;  
   
    //CONSTRUCTORES
    public Articulo() {
    }
    
    public Articulo(int id, String nombre, int stock){
        this.id = id;
        this.nombre = nombre;
        this.stock = stock;
    }
    
    //GETTERS & SETTERS
    public Articulo (String nombre, int stock){
        
        this.nombre = nombre;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
    
    
}
