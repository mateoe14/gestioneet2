/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.io;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author root
 */

@Entity
@Table (name = "talleres")
public class Taller implements Serializable {
    
    //@Basic(optional = false)
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", unique = true, updatable = false, nullable = false)
    private int id;
        
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "año_curso")
    private int añoCurso;      
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="dni_profesor", nullable = true)
    private Profesor profesor;
   
//    @OneToMany(mappedBy = "taller")
//    private List<Cursada> cursadas;
    
    public Taller (){}
    
    public Taller (String nombre, int añoCurso, Profesor profesor){        
        
        this.nombre = nombre;
        this.añoCurso = añoCurso;
        this.profesor = profesor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAñoCurso() {
        return añoCurso;
    }

    public void setAñoCurso(int añoCurso) {
        this.añoCurso = añoCurso;
    }

    public Profesor getProfesor() {
        
        try {
        this.profesor.toString();} catch (Exception e) {}
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

//    public List<Cursada> getCursadas() {
//        return cursadas;
//    }
//
//    public void setCursadas(List<Cursada> cursadas) {
//        this.cursadas = cursadas;
//    }    
    
}
