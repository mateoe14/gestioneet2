/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.controller;

import eet2.dao.InventarioDao;
import eet2.dao.InventarioDaoImpl;
import eet2.io.Articulo;
import java.util.List;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class GestorInventario {
    
    //ATRIBUTOS
    private final InventarioDao articulosDAO;
    private final SessionFactory sessionFactory;
    
    //CONSTRUCTOR
    public GestorInventario(SessionFactory sessionFactory){
    
        this.sessionFactory = sessionFactory;
    
    // creamos las instancias de los objetos de acceso a datos (DAO)
        this.articulosDAO = new InventarioDaoImpl(sessionFactory);
    }
    
        
    public void agregarArticulo(Articulo unArticulo){
    
        this.articulosDAO.agregar(unArticulo);
        
    }
    
    public List<Articulo> buscarArticuloPorNombre(String nombre){
        
        return this.articulosDAO.buscarPorNombre(nombre);
        
    }
    
    public List<Articulo> listarArticulos(){
        
        return this.articulosDAO.listar();
    }
    
    public void modificarArticulo(Articulo unArticulo){
        
        this.articulosDAO.modificar(unArticulo);
        
    }
    
    public void eliminarArticulo(Articulo unArticulo){
        
        this.articulosDAO.eliminar(unArticulo);
        
    }
    
}
