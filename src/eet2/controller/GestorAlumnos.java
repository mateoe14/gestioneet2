/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.controller;

import eet2.dao.AlumnosDao;
import eet2.dao.AlumnosDaoImpl;
import eet2.io.Alumno;
import java.util.List;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class GestorAlumnos {
    
    private final SessionFactory sessionFactory;
    private final AlumnosDao alumnosDAO;
    
    public GestorAlumnos (SessionFactory sessionFactory){
        
        this.sessionFactory = sessionFactory;
        
        this.alumnosDAO = new AlumnosDaoImpl(sessionFactory);
    }
    
    public void agregarAlumno(Alumno unAlumno){
        this.alumnosDAO.agregar(unAlumno);
    }
    
    public void modificarAlumno(Alumno unAlumno){
        this.alumnosDAO.modificar(unAlumno);
    }
    
    public void eliminarAlumno(Alumno unAlumno){
        this.alumnosDAO.eliminar(unAlumno);
    }
    
    public Alumno buscarAlumnoPorId(int dni){
        return this.alumnosDAO.buscarPorId(dni);
    }
    
    public List<Alumno> buscarAlumnoPorNombre(String nombre){
        return this.alumnosDAO.buscarPorNombre(nombre);
    }
    
    public List<Alumno> buscarAlumnoPorApellido(String apellido){
        return this.alumnosDAO.buscarPorApellido(apellido);
    }
    
    public List<Alumno> listarAlumnos(){
        return this.alumnosDAO.listar();
    }
    
}
