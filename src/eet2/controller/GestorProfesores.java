/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.controller;

import eet2.dao.ProfesoresDao;
import eet2.dao.ProfesoresDaoImpl;
import eet2.io.Profesor;
import java.util.List;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class GestorProfesores {
    
    //ATRIBUTOS
    private final SessionFactory sessionFactory;
    
    private final ProfesoresDao profesoresDAO;
    
    public GestorProfesores (SessionFactory sessionFactory){
        
        this.sessionFactory = sessionFactory;
        
        // creamos las instancias de los objetos de acceso a datos (DAO)
        this.profesoresDAO = new ProfesoresDaoImpl(sessionFactory);
    }
    
    public void agregarProfesor (Profesor unProfesor){
        this.profesoresDAO.agregar(unProfesor);
    }
    
    public void modificarProfesor (Profesor unProfesor){
        this.profesoresDAO.modificar(unProfesor);
    }
    
    public void eliminarProfesor (Profesor unProfesor){
        this.profesoresDAO.eliminar(unProfesor);
    }
    
    public List<Profesor> buscarProfesorPorNombre (String nombre){
        return this.profesoresDAO.buscarPorNombre(nombre);
    }
    
    public List<Profesor> buscarProfesorPorApellido (String apellido){
        return this.profesoresDAO.buscarPorApellido(apellido);
    }
    
    public Profesor buscarProfesorPorId(int id) {
        return this.profesoresDAO.buscarPorId(id);
    }
    
    public List<Profesor> listarProfesores(){
        return this.profesoresDAO.listar();
    }
    
}
