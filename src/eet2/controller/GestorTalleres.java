/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.controller;

import eet2.dao.ProfesoresDao;
import eet2.dao.ProfesoresDaoImpl;
import eet2.dao.TalleresDao;
import eet2.dao.TalleresDaoImpl;
import eet2.io.Profesor;
import eet2.io.Taller;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author root
 */
public class GestorTalleres {
    
    private final SessionFactory sessionFactory;
    private TalleresDao talleresDAO;
    private ProfesoresDao profesoresDAO;
    
    public GestorTalleres (SessionFactory sessionFactory){        
        this.sessionFactory = sessionFactory;
        this.talleresDAO = new TalleresDaoImpl(sessionFactory);
        this.profesoresDAO = new ProfesoresDaoImpl(sessionFactory);
    }
    
    public void agregarTaller (Taller unTaller){
        this.talleresDAO.agregar(unTaller);
    }
    
    public void modificarTaller(Taller unTaller){        
        this.talleresDAO.modificar(unTaller);        
    }
    
    public void eliminarTaller(Taller unTaller){        
        this.talleresDAO.eliminar(unTaller);        
    }
    
    public Taller buscarTallerPorId(int id){
        return this.talleresDAO.buscarPorId(id);
    }
    
    public List<Taller> buscarTallerPorNombre(String nombre){        
        return this.talleresDAO.buscarPorNombre(nombre);        
    }    
    
    public List<Taller> buscarTallerPorAño(int año){        
        return this.talleresDAO.buscarPorAño(año);        
    }
    
    public List<Taller> listarTalleres(){        
        return this.talleresDAO.listar();
    }
    
    public List<Profesor> obtenerProfesores(){
        return this.profesoresDAO.listar();
    }
    
}
