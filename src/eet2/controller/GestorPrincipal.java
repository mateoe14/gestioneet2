/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eet2.controller;

import eet2.ui.PantallaPrincipal;
import org.hibernate.SessionFactory;
/**
 *
 * @author root
 */
public class GestorPrincipal {
    
    private SessionFactory sessionFactory = null;

    public void run(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
        new PantallaPrincipal(sessionFactory).setVisible(true);
    }

}
